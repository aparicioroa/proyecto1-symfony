<?php

/**
 * manejarprofesores actions.
 *
 * @package    sf_sandbox
 * @subpackage manejarprofesores
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class manejarprofesoresActions extends sfActions
{


  public function executeIndex()
  {
    $this->forward('manejarprofesores', 'crear');
  }

  public function executeCrear()
  {
      $this->msj = '';
    if ($this->getRequest()->getMethod() == sfRequest::POST)
    {

        if($this->getRequestParameter('cedula') &&$this->getRequestParameter('nombre')){

            $profesores = new Profesores();

            $profesores->setCedula($this->getRequestParameter('cedula'));
            $profesores->setNombre($this->getRequestParameter('nombre'));
            $profesores->setApellido($this->getRequestParameter('apellido'));
            $profesores->setFechan($this->getRequestParameter('fecha'));
            
            $profesores->save();

            $this->msj = 'Profesor Creado';

        }else{
            $this->msj = 'Debe insertar al menos la cedula y el nombre de profesor';
        }
    }
  }
  public function executeBuscar()
  {
    $this->profesores = array();
    if ($this->getRequest()->getMethod() == sfRequest::POST)
    {
        $c = new Criteria();

        $c->add(ProfesoresPeer::NOMBRE,'%'.$this->getRequestParameter('nombre').'%',Criteria::LIKE);

        $this->profesores = ProfesoresPeer::doSelect($c);
        
    }

  }


 public function handleErrorCrear()
    {
        $this->msj = 'Error!';
        // Mostrar el formulario
        return sfView::SUCCESS;
    }

public function validateCrear()
    {
        $fecha = $this->getRequestParameter('fecha');

        $añoactual = date('Y');
        $anofecha = date('Y',strtotime($fecha));
        if(($añoactual-$anofecha)>=18) return true;
        else{
            $this->getRequest()->setError('fecha', 'El profesor no puede tener menos de 18 años');
            return false;
        }

    }


}
