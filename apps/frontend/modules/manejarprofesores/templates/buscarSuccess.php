<h2>Buscar Profesores</h2>
<?php echo form_tag('manejarprofesores/buscar') ?>
  <?php echo label_for('labelnombre', 'Nombre') ?>
  <?php echo input_tag('nombre') ?>
  <?php echo submit_tag('Buscar') ?>
</form>
<br>
<h2><?php echo count($profesores).' Profesores encontrados.' ?></h2>
<br>
<br>

<?php foreach($profesores as $prof) : ?>
<p><?php echo $prof->getCedula().' - '.$prof->getNombre().' '.$prof->getApellido() ?></p>
<br>
<?php endforeach ?>