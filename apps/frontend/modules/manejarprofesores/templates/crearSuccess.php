<?php use_helper('Validation') ?>
<?php if ($sf_request->hasErrors()): ?>
  <p>Los datos ingresados contienen errores:</p>
  <ul>
    <?php foreach($sf_request->getErrors() as $error): ?>
        <li><?php echo $error ?></li>
    <?php endforeach ?>
 </ul>
<?php endif ?>


<h2>Creando Profesores</h2>
<?php echo form_tag('manejarprofesores/crear') ?>
  <?php echo form_error('cedula') ?><br />
  <?php echo label_for('labelcedula', 'Cédula') ?>
  <?php echo input_tag('cedula') ?>
   
   
  <br>
  <?php echo form_error('nombre') ?><br />
  <?php echo label_for('labelnombre', 'Nombre') ?>
  <?php echo input_tag('nombre') ?>
   
   
  <br>
  <?php echo form_error('apellido') ?><br />
  <?php echo label_for('labelapellido', 'Apellido') ?>
  <?php echo input_tag('apellido') ?>
   
   
  <br>
  <?php echo form_error('fecha') ?><br />
  <?php echo label_for('labelnac', 'Fecha Nacimiento') ?>
  <?php echo input_date_tag('fecha','','rich=true') ?>
   
   
  <br>

  <?php echo submit_tag('Insertar') ?>
</form>

<h2><?php if($msj!='') echo $msj ?></h2>


