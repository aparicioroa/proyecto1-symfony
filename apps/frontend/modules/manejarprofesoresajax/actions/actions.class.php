<?php

/**
 * manejarprofesoresajax actions.
 *
 * @package    sf_sandbox
 * @subpackage manejarprofesoresajax
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class manejarprofesoresajaxActions extends sfActions
{
  

public function executeIndex()
    {
      $this->forward('manejarprofesoresajax', 'crear');
    }

    public function executeCrear()
    {
        
    }

    public function executeInsertar()
    {
      if($this->getRequestParameter('cedula') && $this->getRequestParameter('nombre')){

          $profesores = new Profesores();

          $profesores->setCedula($this->getRequestParameter('cedula'));
          $profesores->setNombre($this->getRequestParameter('nombre'));
          $profesores->setApellido($this->getRequestParameter('apellido'));
          $profesores->setFechan($this->getRequestParameter('fecha'));

          $profesores->save();

          $this->msj = 'Profesor Creado';
      }else{
          $this->msj = 'Debe insertar al menos la cedula y el nombre de profesor';
      }
    }

    public function executeBuscar()
    {

    }

     public function executeResultados()
     {
       $this->profesores = array();
       if ($this->getRequest()->getMethod() == sfRequest::POST)
       {
           $c = new Criteria();

           $c->add(ProfesoresPeer::NOMBRE,'%'.$this->getRequestParameter('nombre').'%',Criteria::LIKE);

           $this->profesores = ProfesoresPeer::doSelect($c);
       }
     }

     public function executeEliminar()
     {
         $profesor = ProfesoresPeer::retrieveByPK($this->getRequestParameter('id'));
         if($profesor){
             $profesor->delete();
             $this->eliminado=true;
         }else $this->eliminado=false;
     }

		public function executeValidar()
		{
		    $c = new Criteria();
		    $c->add(ProfesoresPeer::CEDULA,$this->getRequestParameter('cedula'));
		    $profesor = ProfesoresPeer::doSelectOne($c);
		    if($profesor){
		      $this->msj = 'La cedula del profesor Existe';
		    }else $this->msj = 'Cedula Válida';
		}
      
  
}
