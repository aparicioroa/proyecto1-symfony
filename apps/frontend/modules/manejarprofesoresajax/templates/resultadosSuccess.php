<?php echo use_helper('Javascript') ?>

<h2><?php echo count($profesores).' Profesores encontrados.' ?></h2>
<br>
<br>
<?php foreach($profesores as $prof) : ?>
<p id="div<?php echo $prof->getId() ?>"><?php echo $prof->getCedula().' - '.$prof->getNombre().' '.$prof->getApellido() ?>
    <?php echo link_to_remote('Eliminar',array('update' => 'div'.$prof->getId(), 'url' => 'manejarprofesoresajax/eliminar?id='.$prof->getId())) ?>
</p>
<br>
<?php endforeach ?>
