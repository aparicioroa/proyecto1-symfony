<?php echo use_helper('Javascript') ?>

<h2>Creando Profesores</h2>
<?php echo form_remote_tag(array('url' => 'manejarprofesoresajax/insertar', 'update' => 'divcreado' )) ?>


 <?php echo label_for('labelcedula', 'Cédula') ?>
 <?php echo input_tag('cedula') ?>

 <br>
 <?php echo label_for('labelnombre', 'Nombre') ?>
 <?php echo input_tag('nombre') ?>
 <br>
 <?php echo label_for('labelapellido', 'Apellido') ?>
 <?php echo input_tag('apellido') ?>
 <br>
 <?php echo label_for('labelnac', 'Fecha Nacimiento') ?>
 <?php echo input_date_tag('fecha','','rich=true') ?>
 <br>
 <?php echo submit_tag('Crear Profesor') ?>

 <?php echo observe_field('cedula', array(
  'update'   => 'divcedula',
  'url'      => 'manejarprofesoresajax/validar',
  'with'     => "'cedula='+$('cedula').value",
  'script'   => true,
)) ?>
</form>
<div id="divcreado"></div> 
<div id="divcedula"></div>