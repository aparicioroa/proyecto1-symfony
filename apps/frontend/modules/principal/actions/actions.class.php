<?php

/**
 * principal actions.
 *
 * @package    sf_sandbox
 * @subpackage principal
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class principalActions extends sfActions
{
  /**
   * Executes index action
   *
   */
  public function executeIndex()
  {
    $hoy = getdate();
    $this->hora = $hoy['hours'];  

    $this->nombre_sesion = $this->getUser()->getAttribute('nombre', '');
  }


    public function executeMostrar()
  {
      $this->nombre = $this->getRequestParameter('nombre');
      $nombre = $this->getUser()->getAttribute('nombre', '');
      if($nombre=='') $this->getUser()->setAttribute('nombre', $this->nombre);
  }


}
