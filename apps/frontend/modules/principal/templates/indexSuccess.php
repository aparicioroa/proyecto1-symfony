<p>¡Hola, Mundo!</p>
<?php if ($hora < 12 ): ?>
<p>Buenos días. Ya son las <?php echo $hora ?>.</p>
<?php elseif($hora > 12 ):?>
<p>Buenos Tardes. Ya son las <?php echo $hora ?>.</p>
<?php endif; ?>
<br/><br/>


<?php echo form_tag('principal/mostrar') ?>
  <?php echo label_for('nombre', '¿Cómo te llamas?') ?>
  <?php echo input_tag('nombre') ?>
  <?php echo submit_tag('Ok') ?>
</form>
<br/><br/>


<?php if($nombre_sesion==""): ?>
<p>No ha ingresado aún ningún nombre al formulario</p>
<?php else: ?>
<p>El primer nombre ingresado al formulario es: <?php echo $nombre_sesion ?></p>
<?php endif; ?>


<?php echo use_helper('Javascript') ?>

<?php echo javascript_tag("
  function MostrarFechaHora()
  {
    Stamp = new Date();
    alert('La fecha y hora actual es: ' + Stamp);
    return true;
  }
") ?>

<?php echo link_to_function('Ver Fecha y Hora Actual', "MostrarFechaHora()") ?>