<?php

/**
 * autenticacion actions.
 *
 * @package    sf_sandbox
 * @subpackage autenticacion
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class autenticacionActions extends sfActions
{
  /**
   * Executes index action
   *
   */
public function executeIndex()
  {
    $this->forward('autenticacion', 'login');
  }

  public function executeLogin()
  {
    $this->error = '';
    if ($this->getRequest()->getMethod() == sfRequest::POST)
    {
      $usuario = new Login();
      // Procesar los datos del formulario
      $login = $this->getRequestParameter('login');
      $pass = md5($this->getRequestParameter('password'));

        $c = new Criteria();
        $c->add(LoginPeer::US,$login);
        $c->add(LoginPeer::PASS,$pass);
        $this->$logueo = LoginPeer::doSelect($c);
        if($this->$logueo){
             
        $this->getUser()->setAuthenticated(true);
        $this->getUser()->addCredential('administrador');

        $this->error = '';

        $this->forward('principal', 'index');

      }else{
        $this->error = 'Error en el login/password';
      } 
        




   
    }
  }

  public function executeLogout()
  {
    if ($this->getUser()->isAuthenticated())
    {
      $this->getUser()->setAuthenticated(false);
    }
  }
}
