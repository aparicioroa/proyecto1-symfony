<?php echo use_helper('Javascript') ?>
<?php if ($sf_request->hasErrors()): ?>
  <p>Los datos ingresados contienen errores:</p>
  <ul>
    <?php foreach($sf_request->getErrors() as $error): ?>
        <li><?php echo $error ?></li>
    <?php endforeach ?>
 </ul>
<?php endif ?>
<h2>Crear Usuarios</h2>

 <?php echo form_tag('crearusuario/crear') ?>

  <?php echo label_for('labelus', 'Usuario') ?>
  <?php echo input_tag('us') ?>

<?php echo observe_field('us', array(
  'update'   => 'divlog',
  'url'      => 'crearusuario/validar',
  'with'     => "'us='+$('us').value",
  'script'   => true,
)) ?>
  <br>
  <?php echo label_for('labelpass', 'Password') ?>
  <?php echo input_password_tag('pass') ?>
  
  <br> 
  <?php echo submit_tag('Insertar') ?>
</form>
<h2><?php if($msj!='') echo $msj ?></h2>
<div id="divlog"></div>

