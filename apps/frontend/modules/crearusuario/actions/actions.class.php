<?php

/**
 * crearusuario actions.
 *
 * @package    sf_sandbox
 * @subpackage crearusuario
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2692 2006-11-15 21:03:55Z fabien $
 */
class crearusuarioActions extends sfActions
{
  /**
   * Executes index action
   *
   */
	public function executeIndex()
  {
    $this->forward('crearusuario', 'crear');
  }

  public function executeCrear()
  {
      $this->msj = '';
    if ($this->getRequest()->getMethod() == sfRequest::POST)
    {

        if($this->getRequestParameter('us') &&$this->getRequestParameter('pass')){

            $login = new Login();

            $login->setUs($this->getRequestParameter('us'));
            $login->setPass(md5($this->getRequestParameter('pass')));
            
            
            $login->save();

            $this->msj = 'Usuario Creado';

        }else{
            $this->msj = 'Inserte los campos requeridos';
        }
    }
  }
    public function executeValidar(){
    $c = new Criteria();
    $c->add(LoginPeer::US,$this->getRequestParameter('us'));
    $login = LoginPeer::doSelectOne($c);
    if($login){
      $this->msj = 'El nombre de usuario ya existe!.';
    }else $this->msj = 'Usuario Válido';
    }

  public function executeBuscar()
  {
  }

  public function executeResultados()
     {
       $this->login = array();
       if ($this->getRequest()->getMethod() == sfRequest::POST)
       {
           $c = new Criteria();

           $c->add(LoginPeer::US,'%'.$this->getRequestParameter('us').'%',Criteria::LIKE);

           $this->login = LoginPeer::doSelect($c);
       }
     }
    public function handleErrorCrear()
    {
        $this->msj = "Error!";
        // Mostrar el formulario
        return sfView::SUCCESS;
    }

    public function executeEliminar()
     {
         $login = LoginPeer::retrieveByPK($this->getRequestParameter('id'));
         if($login){
             $login->delete();
             $this->eliminado=true;
         }else $this->eliminado=false;
     }


}
