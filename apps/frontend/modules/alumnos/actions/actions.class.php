<?php

/**
 * alumnos actions.
 *
 * @package    sf_sandbox
 * @subpackage alumnos
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 2288 2006-10-02 15:22:13Z fabien $
 */
class alumnosActions extends autoalumnosActions
{

	   public function executeEdad()
    {
        $alumnos = AlumnosPeer::retrieveByPK($this->getRequestParameter('id'));

        if($alumnos){
            $fecha = $alumnos->getFechan();

            $añoactual = date('Y');
            $anofecha = date('Y',strtotime($fecha));
            $this->edad = ($añoactual-$anofecha);
        }else $this->edad = 'Alumno no encontrado';

    }


	
}
