<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<?php include_http_metas() ?>
<?php include_metas() ?>

<?php include_title() ?>

<link rel="shortcut icon" href="/favicon.ico" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<link href="fonts.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>

		<div id="menu">
<ul> 
			<li class="current_page_item">
				<?php echo link_to('Home','principal/index') ?>
			</li> 
			<li>
				<?php echo link_to('Alumnos','alumnos/index') ?>
			</li> 
			<li>
				<?php echo link_to('Profesores','profesores/index') ?>
			</li> 
			<li>
				<?php echo link_to('Materias','materias/index') ?>
			</li> 
			<li>
				<?php echo link_to('Secciones','secciones/index') ?>
			</li> 
			<li>
				<?php echo link_to('Inscripciones','inscripciones/index') ?>
			</li> 
		</ul>
		</div>
	</div>
	<div id="banner" class="container">
		<div class="title">
			<h1>Inducción Symfony 1.0</h1>
		</div>

	</div>
</div>
<div id="wrapper">
	<?php echo $sf_data->getRaw('sf_content') ?>
	</div>
</div>

</body>



</html>
