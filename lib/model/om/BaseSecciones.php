<?php


abstract class BaseSecciones extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $codigo;


	
	protected $nombre;


	
	protected $profesores_id;


	
	protected $materias_id;


	
	protected $id;

	
	protected $aProfesores;

	
	protected $aMaterias;

	
	protected $collInscripcioness;

	
	protected $lastInscripcionesCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getCodigo()
	{

		return $this->codigo;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getProfesoresId()
	{

		return $this->profesores_id;
	}

	
	public function getMateriasId()
	{

		return $this->materias_id;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setCodigo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->codigo !== $v) {
			$this->codigo = $v;
			$this->modifiedColumns[] = SeccionesPeer::CODIGO;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = SeccionesPeer::NOMBRE;
		}

	} 
	
	public function setProfesoresId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->profesores_id !== $v) {
			$this->profesores_id = $v;
			$this->modifiedColumns[] = SeccionesPeer::PROFESORES_ID;
		}

		if ($this->aProfesores !== null && $this->aProfesores->getId() !== $v) {
			$this->aProfesores = null;
		}

	} 
	
	public function setMateriasId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->materias_id !== $v) {
			$this->materias_id = $v;
			$this->modifiedColumns[] = SeccionesPeer::MATERIAS_ID;
		}

		if ($this->aMaterias !== null && $this->aMaterias->getId() !== $v) {
			$this->aMaterias = null;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = SeccionesPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->codigo = $rs->getString($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->profesores_id = $rs->getInt($startcol + 2);

			$this->materias_id = $rs->getInt($startcol + 3);

			$this->id = $rs->getInt($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Secciones object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SeccionesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			SeccionesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SeccionesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aProfesores !== null) {
				if ($this->aProfesores->isModified()) {
					$affectedRows += $this->aProfesores->save($con);
				}
				$this->setProfesores($this->aProfesores);
			}

			if ($this->aMaterias !== null) {
				if ($this->aMaterias->isModified()) {
					$affectedRows += $this->aMaterias->save($con);
				}
				$this->setMaterias($this->aMaterias);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = SeccionesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += SeccionesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collInscripcioness !== null) {
				foreach($this->collInscripcioness as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aProfesores !== null) {
				if (!$this->aProfesores->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aProfesores->getValidationFailures());
				}
			}

			if ($this->aMaterias !== null) {
				if (!$this->aMaterias->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMaterias->getValidationFailures());
				}
			}


			if (($retval = SeccionesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collInscripcioness !== null) {
					foreach($this->collInscripcioness as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SeccionesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getCodigo();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getProfesoresId();
				break;
			case 3:
				return $this->getMateriasId();
				break;
			case 4:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SeccionesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getCodigo(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getProfesoresId(),
			$keys[3] => $this->getMateriasId(),
			$keys[4] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SeccionesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setCodigo($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setProfesoresId($value);
				break;
			case 3:
				$this->setMateriasId($value);
				break;
			case 4:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SeccionesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setCodigo($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setProfesoresId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setMateriasId($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setId($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(SeccionesPeer::DATABASE_NAME);

		if ($this->isColumnModified(SeccionesPeer::CODIGO)) $criteria->add(SeccionesPeer::CODIGO, $this->codigo);
		if ($this->isColumnModified(SeccionesPeer::NOMBRE)) $criteria->add(SeccionesPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(SeccionesPeer::PROFESORES_ID)) $criteria->add(SeccionesPeer::PROFESORES_ID, $this->profesores_id);
		if ($this->isColumnModified(SeccionesPeer::MATERIAS_ID)) $criteria->add(SeccionesPeer::MATERIAS_ID, $this->materias_id);
		if ($this->isColumnModified(SeccionesPeer::ID)) $criteria->add(SeccionesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SeccionesPeer::DATABASE_NAME);

		$criteria->add(SeccionesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCodigo($this->codigo);

		$copyObj->setNombre($this->nombre);

		$copyObj->setProfesoresId($this->profesores_id);

		$copyObj->setMateriasId($this->materias_id);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getInscripcioness() as $relObj) {
				$copyObj->addInscripciones($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SeccionesPeer();
		}
		return self::$peer;
	}

	
	public function setProfesores($v)
	{


		if ($v === null) {
			$this->setProfesoresId(NULL);
		} else {
			$this->setProfesoresId($v->getId());
		}


		$this->aProfesores = $v;
	}


	
	public function getProfesores($con = null)
	{
		if ($this->aProfesores === null && ($this->profesores_id !== null)) {
						include_once 'lib/model/om/BaseProfesoresPeer.php';

			$this->aProfesores = ProfesoresPeer::retrieveByPK($this->profesores_id, $con);

			
		}
		return $this->aProfesores;
	}

	
	public function setMaterias($v)
	{


		if ($v === null) {
			$this->setMateriasId(NULL);
		} else {
			$this->setMateriasId($v->getId());
		}


		$this->aMaterias = $v;
	}


	
	public function getMaterias($con = null)
	{
		if ($this->aMaterias === null && ($this->materias_id !== null)) {
						include_once 'lib/model/om/BaseMateriasPeer.php';

			$this->aMaterias = MateriasPeer::retrieveByPK($this->materias_id, $con);

			
		}
		return $this->aMaterias;
	}

	
	public function initInscripcioness()
	{
		if ($this->collInscripcioness === null) {
			$this->collInscripcioness = array();
		}
	}

	
	public function getInscripcioness($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseInscripcionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInscripcioness === null) {
			if ($this->isNew()) {
			   $this->collInscripcioness = array();
			} else {

				$criteria->add(InscripcionesPeer::SECCIONES_ID, $this->getId());

				InscripcionesPeer::addSelectColumns($criteria);
				$this->collInscripcioness = InscripcionesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(InscripcionesPeer::SECCIONES_ID, $this->getId());

				InscripcionesPeer::addSelectColumns($criteria);
				if (!isset($this->lastInscripcionesCriteria) || !$this->lastInscripcionesCriteria->equals($criteria)) {
					$this->collInscripcioness = InscripcionesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastInscripcionesCriteria = $criteria;
		return $this->collInscripcioness;
	}

	
	public function countInscripcioness($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseInscripcionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(InscripcionesPeer::SECCIONES_ID, $this->getId());

		return InscripcionesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addInscripciones(Inscripciones $l)
	{
		$this->collInscripcioness[] = $l;
		$l->setSecciones($this);
	}


	
	public function getInscripcionessJoinAlumnos($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseInscripcionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInscripcioness === null) {
			if ($this->isNew()) {
				$this->collInscripcioness = array();
			} else {

				$criteria->add(InscripcionesPeer::SECCIONES_ID, $this->getId());

				$this->collInscripcioness = InscripcionesPeer::doSelectJoinAlumnos($criteria, $con);
			}
		} else {
									
			$criteria->add(InscripcionesPeer::SECCIONES_ID, $this->getId());

			if (!isset($this->lastInscripcionesCriteria) || !$this->lastInscripcionesCriteria->equals($criteria)) {
				$this->collInscripcioness = InscripcionesPeer::doSelectJoinAlumnos($criteria, $con);
			}
		}
		$this->lastInscripcionesCriteria = $criteria;

		return $this->collInscripcioness;
	}

} 