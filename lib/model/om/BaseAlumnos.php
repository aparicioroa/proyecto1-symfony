<?php


abstract class BaseAlumnos extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $cedula;


	
	protected $nombre;


	
	protected $apellido;


	
	protected $fechan;


	
	protected $sexo;


	
	protected $direccion;


	
	protected $telefono;


	
	protected $correo;


	
	protected $especialidad;


	
	protected $lugarnaci;


	
	protected $id;

	
	protected $collInscripcioness;

	
	protected $lastInscripcionesCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getCedula()
	{

		return $this->cedula;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getApellido()
	{

		return $this->apellido;
	}

	
	public function getFechan($format = 'Y-m-d')
	{

		if ($this->fechan === null || $this->fechan === '') {
			return null;
		} elseif (!is_int($this->fechan)) {
						$ts = strtotime($this->fechan);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [fechan] as date/time value: " . var_export($this->fechan, true));
			}
		} else {
			$ts = $this->fechan;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getSexo()
	{

		return $this->sexo;
	}

	
	public function getDireccion()
	{

		return $this->direccion;
	}

	
	public function getTelefono()
	{

		return $this->telefono;
	}

	
	public function getCorreo()
	{

		return $this->correo;
	}

	
	public function getEspecialidad()
	{

		return $this->especialidad;
	}

	
	public function getLugarnaci()
	{

		return $this->lugarnaci;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setCedula($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->cedula !== $v) {
			$this->cedula = $v;
			$this->modifiedColumns[] = AlumnosPeer::CEDULA;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = AlumnosPeer::NOMBRE;
		}

	} 
	
	public function setApellido($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido !== $v) {
			$this->apellido = $v;
			$this->modifiedColumns[] = AlumnosPeer::APELLIDO;
		}

	} 
	
	public function setFechan($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [fechan] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fechan !== $ts) {
			$this->fechan = $ts;
			$this->modifiedColumns[] = AlumnosPeer::FECHAN;
		}

	} 
	
	public function setSexo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->sexo !== $v) {
			$this->sexo = $v;
			$this->modifiedColumns[] = AlumnosPeer::SEXO;
		}

	} 
	
	public function setDireccion($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->direccion !== $v) {
			$this->direccion = $v;
			$this->modifiedColumns[] = AlumnosPeer::DIRECCION;
		}

	} 
	
	public function setTelefono($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->telefono !== $v) {
			$this->telefono = $v;
			$this->modifiedColumns[] = AlumnosPeer::TELEFONO;
		}

	} 
	
	public function setCorreo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->correo !== $v) {
			$this->correo = $v;
			$this->modifiedColumns[] = AlumnosPeer::CORREO;
		}

	} 
	
	public function setEspecialidad($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->especialidad !== $v) {
			$this->especialidad = $v;
			$this->modifiedColumns[] = AlumnosPeer::ESPECIALIDAD;
		}

	} 
	
	public function setLugarnaci($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->lugarnaci !== $v) {
			$this->lugarnaci = $v;
			$this->modifiedColumns[] = AlumnosPeer::LUGARNACI;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = AlumnosPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->cedula = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->apellido = $rs->getString($startcol + 2);

			$this->fechan = $rs->getDate($startcol + 3, null);

			$this->sexo = $rs->getString($startcol + 4);

			$this->direccion = $rs->getString($startcol + 5);

			$this->telefono = $rs->getString($startcol + 6);

			$this->correo = $rs->getString($startcol + 7);

			$this->especialidad = $rs->getString($startcol + 8);

			$this->lugarnaci = $rs->getString($startcol + 9);

			$this->id = $rs->getInt($startcol + 10);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 11; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Alumnos object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AlumnosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			AlumnosPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(AlumnosPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = AlumnosPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += AlumnosPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collInscripcioness !== null) {
				foreach($this->collInscripcioness as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = AlumnosPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collInscripcioness !== null) {
					foreach($this->collInscripcioness as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AlumnosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getCedula();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getApellido();
				break;
			case 3:
				return $this->getFechan();
				break;
			case 4:
				return $this->getSexo();
				break;
			case 5:
				return $this->getDireccion();
				break;
			case 6:
				return $this->getTelefono();
				break;
			case 7:
				return $this->getCorreo();
				break;
			case 8:
				return $this->getEspecialidad();
				break;
			case 9:
				return $this->getLugarnaci();
				break;
			case 10:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AlumnosPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getCedula(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getApellido(),
			$keys[3] => $this->getFechan(),
			$keys[4] => $this->getSexo(),
			$keys[5] => $this->getDireccion(),
			$keys[6] => $this->getTelefono(),
			$keys[7] => $this->getCorreo(),
			$keys[8] => $this->getEspecialidad(),
			$keys[9] => $this->getLugarnaci(),
			$keys[10] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = AlumnosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setCedula($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setApellido($value);
				break;
			case 3:
				$this->setFechan($value);
				break;
			case 4:
				$this->setSexo($value);
				break;
			case 5:
				$this->setDireccion($value);
				break;
			case 6:
				$this->setTelefono($value);
				break;
			case 7:
				$this->setCorreo($value);
				break;
			case 8:
				$this->setEspecialidad($value);
				break;
			case 9:
				$this->setLugarnaci($value);
				break;
			case 10:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = AlumnosPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setCedula($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setApellido($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setFechan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setSexo($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setDireccion($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTelefono($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setCorreo($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setEspecialidad($arr[$keys[8]]);
		if (array_key_exists($keys[9], $arr)) $this->setLugarnaci($arr[$keys[9]]);
		if (array_key_exists($keys[10], $arr)) $this->setId($arr[$keys[10]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(AlumnosPeer::DATABASE_NAME);

		if ($this->isColumnModified(AlumnosPeer::CEDULA)) $criteria->add(AlumnosPeer::CEDULA, $this->cedula);
		if ($this->isColumnModified(AlumnosPeer::NOMBRE)) $criteria->add(AlumnosPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(AlumnosPeer::APELLIDO)) $criteria->add(AlumnosPeer::APELLIDO, $this->apellido);
		if ($this->isColumnModified(AlumnosPeer::FECHAN)) $criteria->add(AlumnosPeer::FECHAN, $this->fechan);
		if ($this->isColumnModified(AlumnosPeer::SEXO)) $criteria->add(AlumnosPeer::SEXO, $this->sexo);
		if ($this->isColumnModified(AlumnosPeer::DIRECCION)) $criteria->add(AlumnosPeer::DIRECCION, $this->direccion);
		if ($this->isColumnModified(AlumnosPeer::TELEFONO)) $criteria->add(AlumnosPeer::TELEFONO, $this->telefono);
		if ($this->isColumnModified(AlumnosPeer::CORREO)) $criteria->add(AlumnosPeer::CORREO, $this->correo);
		if ($this->isColumnModified(AlumnosPeer::ESPECIALIDAD)) $criteria->add(AlumnosPeer::ESPECIALIDAD, $this->especialidad);
		if ($this->isColumnModified(AlumnosPeer::LUGARNACI)) $criteria->add(AlumnosPeer::LUGARNACI, $this->lugarnaci);
		if ($this->isColumnModified(AlumnosPeer::ID)) $criteria->add(AlumnosPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(AlumnosPeer::DATABASE_NAME);

		$criteria->add(AlumnosPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCedula($this->cedula);

		$copyObj->setNombre($this->nombre);

		$copyObj->setApellido($this->apellido);

		$copyObj->setFechan($this->fechan);

		$copyObj->setSexo($this->sexo);

		$copyObj->setDireccion($this->direccion);

		$copyObj->setTelefono($this->telefono);

		$copyObj->setCorreo($this->correo);

		$copyObj->setEspecialidad($this->especialidad);

		$copyObj->setLugarnaci($this->lugarnaci);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getInscripcioness() as $relObj) {
				$copyObj->addInscripciones($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new AlumnosPeer();
		}
		return self::$peer;
	}

	
	public function initInscripcioness()
	{
		if ($this->collInscripcioness === null) {
			$this->collInscripcioness = array();
		}
	}

	
	public function getInscripcioness($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseInscripcionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInscripcioness === null) {
			if ($this->isNew()) {
			   $this->collInscripcioness = array();
			} else {

				$criteria->add(InscripcionesPeer::ALUMNOS_ID, $this->getId());

				InscripcionesPeer::addSelectColumns($criteria);
				$this->collInscripcioness = InscripcionesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(InscripcionesPeer::ALUMNOS_ID, $this->getId());

				InscripcionesPeer::addSelectColumns($criteria);
				if (!isset($this->lastInscripcionesCriteria) || !$this->lastInscripcionesCriteria->equals($criteria)) {
					$this->collInscripcioness = InscripcionesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastInscripcionesCriteria = $criteria;
		return $this->collInscripcioness;
	}

	
	public function countInscripcioness($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseInscripcionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(InscripcionesPeer::ALUMNOS_ID, $this->getId());

		return InscripcionesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addInscripciones(Inscripciones $l)
	{
		$this->collInscripcioness[] = $l;
		$l->setAlumnos($this);
	}


	
	public function getInscripcionessJoinSecciones($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseInscripcionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collInscripcioness === null) {
			if ($this->isNew()) {
				$this->collInscripcioness = array();
			} else {

				$criteria->add(InscripcionesPeer::ALUMNOS_ID, $this->getId());

				$this->collInscripcioness = InscripcionesPeer::doSelectJoinSecciones($criteria, $con);
			}
		} else {
									
			$criteria->add(InscripcionesPeer::ALUMNOS_ID, $this->getId());

			if (!isset($this->lastInscripcionesCriteria) || !$this->lastInscripcionesCriteria->equals($criteria)) {
				$this->collInscripcioness = InscripcionesPeer::doSelectJoinSecciones($criteria, $con);
			}
		}
		$this->lastInscripcionesCriteria = $criteria;

		return $this->collInscripcioness;
	}

} 