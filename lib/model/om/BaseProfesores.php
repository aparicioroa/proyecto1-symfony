<?php


abstract class BaseProfesores extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $cedula;


	
	protected $nombre;


	
	protected $apellido;


	
	protected $fechan;


	
	protected $id;

	
	protected $collSeccioness;

	
	protected $lastSeccionesCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getCedula()
	{

		return $this->cedula;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getApellido()
	{

		return $this->apellido;
	}

	
	public function getFechan($format = 'Y-m-d')
	{

		if ($this->fechan === null || $this->fechan === '') {
			return null;
		} elseif (!is_int($this->fechan)) {
						$ts = strtotime($this->fechan);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse value of [fechan] as date/time value: " . var_export($this->fechan, true));
			}
		} else {
			$ts = $this->fechan;
		}
		if ($format === null) {
			return $ts;
		} elseif (strpos($format, '%') !== false) {
			return strftime($format, $ts);
		} else {
			return date($format, $ts);
		}
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setCedula($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->cedula !== $v) {
			$this->cedula = $v;
			$this->modifiedColumns[] = ProfesoresPeer::CEDULA;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = ProfesoresPeer::NOMBRE;
		}

	} 
	
	public function setApellido($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->apellido !== $v) {
			$this->apellido = $v;
			$this->modifiedColumns[] = ProfesoresPeer::APELLIDO;
		}

	} 
	
	public function setFechan($v)
	{

		if ($v !== null && !is_int($v)) {
			$ts = strtotime($v);
			if ($ts === -1 || $ts === false) { 				throw new PropelException("Unable to parse date/time value for [fechan] from input: " . var_export($v, true));
			}
		} else {
			$ts = $v;
		}
		if ($this->fechan !== $ts) {
			$this->fechan = $ts;
			$this->modifiedColumns[] = ProfesoresPeer::FECHAN;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = ProfesoresPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->cedula = $rs->getInt($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->apellido = $rs->getString($startcol + 2);

			$this->fechan = $rs->getDate($startcol + 3, null);

			$this->id = $rs->getInt($startcol + 4);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 5; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Profesores object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ProfesoresPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			ProfesoresPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(ProfesoresPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = ProfesoresPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += ProfesoresPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSeccioness !== null) {
				foreach($this->collSeccioness as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = ProfesoresPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSeccioness !== null) {
					foreach($this->collSeccioness as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ProfesoresPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getCedula();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getApellido();
				break;
			case 3:
				return $this->getFechan();
				break;
			case 4:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ProfesoresPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getCedula(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getApellido(),
			$keys[3] => $this->getFechan(),
			$keys[4] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = ProfesoresPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setCedula($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setApellido($value);
				break;
			case 3:
				$this->setFechan($value);
				break;
			case 4:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = ProfesoresPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setCedula($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setApellido($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setFechan($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setId($arr[$keys[4]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(ProfesoresPeer::DATABASE_NAME);

		if ($this->isColumnModified(ProfesoresPeer::CEDULA)) $criteria->add(ProfesoresPeer::CEDULA, $this->cedula);
		if ($this->isColumnModified(ProfesoresPeer::NOMBRE)) $criteria->add(ProfesoresPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(ProfesoresPeer::APELLIDO)) $criteria->add(ProfesoresPeer::APELLIDO, $this->apellido);
		if ($this->isColumnModified(ProfesoresPeer::FECHAN)) $criteria->add(ProfesoresPeer::FECHAN, $this->fechan);
		if ($this->isColumnModified(ProfesoresPeer::ID)) $criteria->add(ProfesoresPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(ProfesoresPeer::DATABASE_NAME);

		$criteria->add(ProfesoresPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCedula($this->cedula);

		$copyObj->setNombre($this->nombre);

		$copyObj->setApellido($this->apellido);

		$copyObj->setFechan($this->fechan);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSeccioness() as $relObj) {
				$copyObj->addSecciones($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new ProfesoresPeer();
		}
		return self::$peer;
	}

	
	public function initSeccioness()
	{
		if ($this->collSeccioness === null) {
			$this->collSeccioness = array();
		}
	}

	
	public function getSeccioness($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSeccionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSeccioness === null) {
			if ($this->isNew()) {
			   $this->collSeccioness = array();
			} else {

				$criteria->add(SeccionesPeer::PROFESORES_ID, $this->getId());

				SeccionesPeer::addSelectColumns($criteria);
				$this->collSeccioness = SeccionesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SeccionesPeer::PROFESORES_ID, $this->getId());

				SeccionesPeer::addSelectColumns($criteria);
				if (!isset($this->lastSeccionesCriteria) || !$this->lastSeccionesCriteria->equals($criteria)) {
					$this->collSeccioness = SeccionesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSeccionesCriteria = $criteria;
		return $this->collSeccioness;
	}

	
	public function countSeccioness($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSeccionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SeccionesPeer::PROFESORES_ID, $this->getId());

		return SeccionesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addSecciones(Secciones $l)
	{
		$this->collSeccioness[] = $l;
		$l->setProfesores($this);
	}


	
	public function getSeccionessJoinMaterias($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSeccionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSeccioness === null) {
			if ($this->isNew()) {
				$this->collSeccioness = array();
			} else {

				$criteria->add(SeccionesPeer::PROFESORES_ID, $this->getId());

				$this->collSeccioness = SeccionesPeer::doSelectJoinMaterias($criteria, $con);
			}
		} else {
									
			$criteria->add(SeccionesPeer::PROFESORES_ID, $this->getId());

			if (!isset($this->lastSeccionesCriteria) || !$this->lastSeccionesCriteria->equals($criteria)) {
				$this->collSeccioness = SeccionesPeer::doSelectJoinMaterias($criteria, $con);
			}
		}
		$this->lastSeccionesCriteria = $criteria;

		return $this->collSeccioness;
	}

} 