<?php


abstract class BaseInscripciones extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $secciones_id;


	
	protected $alumnos_id;


	
	protected $id;

	
	protected $aSecciones;

	
	protected $aAlumnos;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getSeccionesId()
	{

		return $this->secciones_id;
	}

	
	public function getAlumnosId()
	{

		return $this->alumnos_id;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setSeccionesId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->secciones_id !== $v) {
			$this->secciones_id = $v;
			$this->modifiedColumns[] = InscripcionesPeer::SECCIONES_ID;
		}

		if ($this->aSecciones !== null && $this->aSecciones->getId() !== $v) {
			$this->aSecciones = null;
		}

	} 
	
	public function setAlumnosId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->alumnos_id !== $v) {
			$this->alumnos_id = $v;
			$this->modifiedColumns[] = InscripcionesPeer::ALUMNOS_ID;
		}

		if ($this->aAlumnos !== null && $this->aAlumnos->getId() !== $v) {
			$this->aAlumnos = null;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = InscripcionesPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->secciones_id = $rs->getInt($startcol + 0);

			$this->alumnos_id = $rs->getInt($startcol + 1);

			$this->id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Inscripciones object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(InscripcionesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			InscripcionesPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(InscripcionesPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


												
			if ($this->aSecciones !== null) {
				if ($this->aSecciones->isModified()) {
					$affectedRows += $this->aSecciones->save($con);
				}
				$this->setSecciones($this->aSecciones);
			}

			if ($this->aAlumnos !== null) {
				if ($this->aAlumnos->isModified()) {
					$affectedRows += $this->aAlumnos->save($con);
				}
				$this->setAlumnos($this->aAlumnos);
			}


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = InscripcionesPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += InscripcionesPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


												
			if ($this->aSecciones !== null) {
				if (!$this->aSecciones->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSecciones->getValidationFailures());
				}
			}

			if ($this->aAlumnos !== null) {
				if (!$this->aAlumnos->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aAlumnos->getValidationFailures());
				}
			}


			if (($retval = InscripcionesPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = InscripcionesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSeccionesId();
				break;
			case 1:
				return $this->getAlumnosId();
				break;
			case 2:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = InscripcionesPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSeccionesId(),
			$keys[1] => $this->getAlumnosId(),
			$keys[2] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = InscripcionesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSeccionesId($value);
				break;
			case 1:
				$this->setAlumnosId($value);
				break;
			case 2:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = InscripcionesPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSeccionesId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setAlumnosId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(InscripcionesPeer::DATABASE_NAME);

		if ($this->isColumnModified(InscripcionesPeer::SECCIONES_ID)) $criteria->add(InscripcionesPeer::SECCIONES_ID, $this->secciones_id);
		if ($this->isColumnModified(InscripcionesPeer::ALUMNOS_ID)) $criteria->add(InscripcionesPeer::ALUMNOS_ID, $this->alumnos_id);
		if ($this->isColumnModified(InscripcionesPeer::ID)) $criteria->add(InscripcionesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(InscripcionesPeer::DATABASE_NAME);

		$criteria->add(InscripcionesPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setSeccionesId($this->secciones_id);

		$copyObj->setAlumnosId($this->alumnos_id);


		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new InscripcionesPeer();
		}
		return self::$peer;
	}

	
	public function setSecciones($v)
	{


		if ($v === null) {
			$this->setSeccionesId(NULL);
		} else {
			$this->setSeccionesId($v->getId());
		}


		$this->aSecciones = $v;
	}


	
	public function getSecciones($con = null)
	{
		if ($this->aSecciones === null && ($this->secciones_id !== null)) {
						include_once 'lib/model/om/BaseSeccionesPeer.php';

			$this->aSecciones = SeccionesPeer::retrieveByPK($this->secciones_id, $con);

			
		}
		return $this->aSecciones;
	}

	
	public function setAlumnos($v)
	{


		if ($v === null) {
			$this->setAlumnosId(NULL);
		} else {
			$this->setAlumnosId($v->getId());
		}


		$this->aAlumnos = $v;
	}


	
	public function getAlumnos($con = null)
	{
		if ($this->aAlumnos === null && ($this->alumnos_id !== null)) {
						include_once 'lib/model/om/BaseAlumnosPeer.php';

			$this->aAlumnos = AlumnosPeer::retrieveByPK($this->alumnos_id, $con);

			
		}
		return $this->aAlumnos;
	}

} 