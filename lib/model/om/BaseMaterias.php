<?php


abstract class BaseMaterias extends BaseObject  implements Persistent {


	
	protected static $peer;


	
	protected $codigo;


	
	protected $nombre;


	
	protected $id;

	
	protected $collSeccioness;

	
	protected $lastSeccionesCriteria = null;

	
	protected $alreadyInSave = false;

	
	protected $alreadyInValidation = false;

	
	public function getCodigo()
	{

		return $this->codigo;
	}

	
	public function getNombre()
	{

		return $this->nombre;
	}

	
	public function getId()
	{

		return $this->id;
	}

	
	public function setCodigo($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->codigo !== $v) {
			$this->codigo = $v;
			$this->modifiedColumns[] = MateriasPeer::CODIGO;
		}

	} 
	
	public function setNombre($v)
	{

						if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->nombre !== $v) {
			$this->nombre = $v;
			$this->modifiedColumns[] = MateriasPeer::NOMBRE;
		}

	} 
	
	public function setId($v)
	{

						if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->id !== $v) {
			$this->id = $v;
			$this->modifiedColumns[] = MateriasPeer::ID;
		}

	} 
	
	public function hydrate(ResultSet $rs, $startcol = 1)
	{
		try {

			$this->codigo = $rs->getString($startcol + 0);

			$this->nombre = $rs->getString($startcol + 1);

			$this->id = $rs->getInt($startcol + 2);

			$this->resetModified();

			$this->setNew(false);

						return $startcol + 3; 
		} catch (Exception $e) {
			throw new PropelException("Error populating Materias object", $e);
		}
	}

	
	public function delete($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MateriasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			MateriasPeer::doDelete($this, $con);
			$this->setDeleted(true);
			$con->commit();
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	public function save($con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(MateriasPeer::DATABASE_NAME);
		}

		try {
			$con->begin();
			$affectedRows = $this->doSave($con);
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollback();
			throw $e;
		}
	}

	
	protected function doSave($con)
	{
		$affectedRows = 0; 		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;


						if ($this->isModified()) {
				if ($this->isNew()) {
					$pk = MateriasPeer::doInsert($this, $con);
					$affectedRows += 1; 										 										 
					$this->setId($pk);  
					$this->setNew(false);
				} else {
					$affectedRows += MateriasPeer::doUpdate($this, $con);
				}
				$this->resetModified(); 			}

			if ($this->collSeccioness !== null) {
				foreach($this->collSeccioness as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;
		}
		return $affectedRows;
	} 
	
	protected $validationFailures = array();

	
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = MateriasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collSeccioness !== null) {
					foreach($this->collSeccioness as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MateriasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->getByPosition($pos);
	}

	
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getCodigo();
				break;
			case 1:
				return $this->getNombre();
				break;
			case 2:
				return $this->getId();
				break;
			default:
				return null;
				break;
		} 	}

	
	public function toArray($keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MateriasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getCodigo(),
			$keys[1] => $this->getNombre(),
			$keys[2] => $this->getId(),
		);
		return $result;
	}

	
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = MateriasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setCodigo($value);
				break;
			case 1:
				$this->setNombre($value);
				break;
			case 2:
				$this->setId($value);
				break;
		} 	}

	
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = MateriasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setCodigo($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setId($arr[$keys[2]]);
	}

	
	public function buildCriteria()
	{
		$criteria = new Criteria(MateriasPeer::DATABASE_NAME);

		if ($this->isColumnModified(MateriasPeer::CODIGO)) $criteria->add(MateriasPeer::CODIGO, $this->codigo);
		if ($this->isColumnModified(MateriasPeer::NOMBRE)) $criteria->add(MateriasPeer::NOMBRE, $this->nombre);
		if ($this->isColumnModified(MateriasPeer::ID)) $criteria->add(MateriasPeer::ID, $this->id);

		return $criteria;
	}

	
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(MateriasPeer::DATABASE_NAME);

		$criteria->add(MateriasPeer::ID, $this->id);

		return $criteria;
	}

	
	public function getPrimaryKey()
	{
		return $this->getId();
	}

	
	public function setPrimaryKey($key)
	{
		$this->setId($key);
	}

	
	public function copyInto($copyObj, $deepCopy = false)
	{

		$copyObj->setCodigo($this->codigo);

		$copyObj->setNombre($this->nombre);


		if ($deepCopy) {
									$copyObj->setNew(false);

			foreach($this->getSeccioness() as $relObj) {
				$copyObj->addSecciones($relObj->copy($deepCopy));
			}

		} 

		$copyObj->setNew(true);

		$copyObj->setId(NULL); 
	}

	
	public function copy($deepCopy = false)
	{
				$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new MateriasPeer();
		}
		return self::$peer;
	}

	
	public function initSeccioness()
	{
		if ($this->collSeccioness === null) {
			$this->collSeccioness = array();
		}
	}

	
	public function getSeccioness($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSeccionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSeccioness === null) {
			if ($this->isNew()) {
			   $this->collSeccioness = array();
			} else {

				$criteria->add(SeccionesPeer::MATERIAS_ID, $this->getId());

				SeccionesPeer::addSelectColumns($criteria);
				$this->collSeccioness = SeccionesPeer::doSelect($criteria, $con);
			}
		} else {
						if (!$this->isNew()) {
												

				$criteria->add(SeccionesPeer::MATERIAS_ID, $this->getId());

				SeccionesPeer::addSelectColumns($criteria);
				if (!isset($this->lastSeccionesCriteria) || !$this->lastSeccionesCriteria->equals($criteria)) {
					$this->collSeccioness = SeccionesPeer::doSelect($criteria, $con);
				}
			}
		}
		$this->lastSeccionesCriteria = $criteria;
		return $this->collSeccioness;
	}

	
	public function countSeccioness($criteria = null, $distinct = false, $con = null)
	{
				include_once 'lib/model/om/BaseSeccionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(SeccionesPeer::MATERIAS_ID, $this->getId());

		return SeccionesPeer::doCount($criteria, $distinct, $con);
	}

	
	public function addSecciones(Secciones $l)
	{
		$this->collSeccioness[] = $l;
		$l->setMaterias($this);
	}


	
	public function getSeccionessJoinProfesores($criteria = null, $con = null)
	{
				include_once 'lib/model/om/BaseSeccionesPeer.php';
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		if ($this->collSeccioness === null) {
			if ($this->isNew()) {
				$this->collSeccioness = array();
			} else {

				$criteria->add(SeccionesPeer::MATERIAS_ID, $this->getId());

				$this->collSeccioness = SeccionesPeer::doSelectJoinProfesores($criteria, $con);
			}
		} else {
									
			$criteria->add(SeccionesPeer::MATERIAS_ID, $this->getId());

			if (!isset($this->lastSeccionesCriteria) || !$this->lastSeccionesCriteria->equals($criteria)) {
				$this->collSeccioness = SeccionesPeer::doSelectJoinProfesores($criteria, $con);
			}
		}
		$this->lastSeccionesCriteria = $criteria;

		return $this->collSeccioness;
	}

} 