<?php



class SeccionesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.SeccionesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('secciones');
		$tMap->setPhpName('Secciones');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('secciones_SEQ');

		$tMap->addColumn('CODIGO', 'Codigo', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addForeignKey('PROFESORES_ID', 'ProfesoresId', 'int', CreoleTypes::INTEGER, 'profesores', 'ID', false, null);

		$tMap->addForeignKey('MATERIAS_ID', 'MateriasId', 'int', CreoleTypes::INTEGER, 'materias', 'ID', false, null);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 