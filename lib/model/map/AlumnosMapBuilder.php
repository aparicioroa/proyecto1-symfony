<?php



class AlumnosMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.AlumnosMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('alumnos');
		$tMap->setPhpName('Alumnos');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('alumnos_SEQ');

		$tMap->addColumn('CEDULA', 'Cedula', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('APELLIDO', 'Apellido', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('FECHAN', 'Fechan', 'int', CreoleTypes::DATE, true, null);

		$tMap->addColumn('SEXO', 'Sexo', 'string', CreoleTypes::CHAR, true, 1);

		$tMap->addColumn('DIRECCION', 'Direccion', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('TELEFONO', 'Telefono', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('CORREO', 'Correo', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('ESPECIALIDAD', 'Especialidad', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('LUGARNACI', 'Lugarnaci', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 