<?php



class LoginMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.LoginMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('login');
		$tMap->setPhpName('Login');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('login_SEQ');

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('US', 'Us', 'string', CreoleTypes::VARCHAR, false, 25);

		$tMap->addColumn('PASS', 'Pass', 'string', CreoleTypes::VARCHAR, false, 25);

	} 
} 