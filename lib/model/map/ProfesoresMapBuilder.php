<?php



class ProfesoresMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.ProfesoresMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('profesores');
		$tMap->setPhpName('Profesores');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('profesores_SEQ');

		$tMap->addColumn('CEDULA', 'Cedula', 'int', CreoleTypes::INTEGER, true, null);

		$tMap->addColumn('NOMBRE', 'Nombre', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('APELLIDO', 'Apellido', 'string', CreoleTypes::VARCHAR, true, 100);

		$tMap->addColumn('FECHAN', 'Fechan', 'int', CreoleTypes::DATE, true, null);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 