<?php



class InscripcionesMapBuilder {

	
	const CLASS_NAME = 'lib.model.map.InscripcionesMapBuilder';

	
	private $dbMap;

	
	public function isBuilt()
	{
		return ($this->dbMap !== null);
	}

	
	public function getDatabaseMap()
	{
		return $this->dbMap;
	}

	
	public function doBuild()
	{
		$this->dbMap = Propel::getDatabaseMap('propel');

		$tMap = $this->dbMap->addTable('inscripciones');
		$tMap->setPhpName('Inscripciones');

		$tMap->setUseIdGenerator(true);

		$tMap->setPrimaryKeyMethodInfo('inscripciones_SEQ');

		$tMap->addForeignKey('SECCIONES_ID', 'SeccionesId', 'int', CreoleTypes::INTEGER, 'secciones', 'ID', false, null);

		$tMap->addForeignKey('ALUMNOS_ID', 'AlumnosId', 'int', CreoleTypes::INTEGER, 'alumnos', 'ID', false, null);

		$tMap->addPrimaryKey('ID', 'Id', 'int', CreoleTypes::INTEGER, true, null);

	} 
} 